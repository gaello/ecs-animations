﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for ScaleTo Tween
/// </summary>
public class ScaleToSystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref ScaleTo scaleTo, ref NonUniformScale scale) =>
        {
            scaleTo.ElapsedTime += Time.deltaTime;

            scale.Value = MathfxHelper.CurvedValueECS(scaleTo.AnimationCurve, scaleTo.StartValue, scaleTo.EndValue, math.clamp((scaleTo.ElapsedTime - scaleTo.Delay) / scaleTo.Duration, 0.0f, 1.0f));

            if (scaleTo.ElapsedTime >= scaleTo.Delay + scaleTo.Duration)
            {
                if (scaleTo.LoopAnimation)
                {
                    scaleTo.ElapsedTime -= scaleTo.Duration;
                }
                else
                {
                    scale.Value = scaleTo.EndValue;
                    PostUpdateCommands.RemoveComponent<ScaleTo>(entity);
                }
            }
        });
    }
}
