﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for RotateBy Tween
/// </summary>
public class RotateBySystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref RotateBy rotateBy, ref Rotation rotation) =>
        {
            var start = MathfxHelper.CurvedValueECS(rotateBy.AnimationCurve, float3.zero, rotateBy.TargetOffset, math.clamp((rotateBy.ElapsedTime - rotateBy.Delay) / rotateBy.Duration, 0.0f, 1.0f));
            var end = MathfxHelper.CurvedValueECS(rotateBy.AnimationCurve, float3.zero, rotateBy.TargetOffset, math.clamp((rotateBy.ElapsedTime - rotateBy.Delay + Time.deltaTime) / rotateBy.Duration, 0.0f, 1.0f));

            rotation.Value = math.mul(math.normalize(rotation.Value), quaternion.Euler(end - start));

            rotateBy.ElapsedTime += Time.deltaTime;

            if (rotateBy.ElapsedTime >= rotateBy.Delay + rotateBy.Duration)
            {
                if (rotateBy.LoopAnimation)
                {
                    rotateBy.ElapsedTime -= rotateBy.Duration;
                }
                else
                {
                    PostUpdateCommands.RemoveComponent<RotateBy>(entity);
                }
            }
        });
    }
}
