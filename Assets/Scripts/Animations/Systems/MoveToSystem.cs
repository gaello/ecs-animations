﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for MoveTo Tween
/// </summary>
public class MoveToSystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref MoveTo moveTo, ref Translation translation) =>
        {
            moveTo.ElapsedTime += Time.deltaTime;

            translation.Value = MathfxHelper.CurvedValueECS(moveTo.AnimationCurve, moveTo.StartValue, moveTo.EndValue, math.clamp((moveTo.ElapsedTime - moveTo.Delay) / moveTo.Duration, 0.0f, 1.0f));

            if (moveTo.ElapsedTime >= moveTo.Delay + moveTo.Duration)
            {
                if (moveTo.LoopAnimation)
                {
                    moveTo.ElapsedTime -= moveTo.Duration;
                }
                else
                {
                    translation.Value = moveTo.EndValue;
                    PostUpdateCommands.RemoveComponent<MoveTo>(entity);
                }
            }
        });
    }
}
