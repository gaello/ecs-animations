﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for ScaleBy Tween
/// </summary>
public class ScaleBySystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref ScaleBy scaleBy, ref NonUniformScale scale) =>
        {
            var start = MathfxHelper.CurvedValueECS(scaleBy.AnimationCurve, float3.zero, scaleBy.TargetOffset, math.clamp((scaleBy.ElapsedTime - scaleBy.Delay) / scaleBy.Duration, 0.0f, 1.0f));
            var end = MathfxHelper.CurvedValueECS(scaleBy.AnimationCurve, float3.zero, scaleBy.TargetOffset, math.clamp((scaleBy.ElapsedTime - scaleBy.Delay + Time.deltaTime) / scaleBy.Duration, 0.0f, 1.0f));

            scale.Value += end - start;

            scaleBy.ElapsedTime += Time.deltaTime;

            if (scaleBy.ElapsedTime >= scaleBy.Delay + scaleBy.Duration)
            {
                if (scaleBy.LoopAnimation)
                {
                    scaleBy.ElapsedTime -= scaleBy.Duration;
                }
                else
                {
                    PostUpdateCommands.RemoveComponent<ScaleBy>(entity);
                }
            }
        });
    }
}
