﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for RotateTo Tween
/// </summary>
public class RotateToSystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref RotateTo rotateTo, ref Rotation rotation) =>
        {
            rotateTo.ElapsedTime += Time.deltaTime;

            var eulerRot = MathfxHelper.CurvedValueECS(rotateTo.AnimationCurve, rotateTo.StartValue, rotateTo.EndValue, math.clamp((rotateTo.ElapsedTime - rotateTo.Delay) / rotateTo.Duration, 0.0f, 1.0f));
            rotation.Value = quaternion.Euler(eulerRot);

            if (rotateTo.ElapsedTime >= rotateTo.Delay + rotateTo.Duration)
            {
                if (rotateTo.LoopAnimation)
                {
                    rotateTo.ElapsedTime -= rotateTo.Duration;
                }
                else
                {
                    rotation.Value = quaternion.Euler(rotateTo.EndValue);
                    PostUpdateCommands.RemoveComponent<RotateTo>(entity);
                }
            }
        });
    }
}
