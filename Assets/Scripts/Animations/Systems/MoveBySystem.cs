﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for MoveBy Tween
/// </summary>
public class MoveBySystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref MoveBy moveBy, ref Translation translation) =>
        {
            var start = MathfxHelper.CurvedValueECS(moveBy.AnimationCurve, float3.zero, moveBy.TargetOffset, math.clamp((moveBy.ElapsedTime - moveBy.Delay) / moveBy.Duration, 0.0f, 1.0f));
            var end = MathfxHelper.CurvedValueECS(moveBy.AnimationCurve, float3.zero, moveBy.TargetOffset, math.clamp((moveBy.ElapsedTime - moveBy.Delay + Time.deltaTime) / moveBy.Duration, 0.0f, 1.0f));

            translation.Value += end - start;

            moveBy.ElapsedTime += Time.deltaTime;

            if (moveBy.ElapsedTime >= moveBy.Delay + moveBy.Duration)
            {
                if (moveBy.LoopAnimation)
                {
                    moveBy.ElapsedTime -= moveBy.Duration;
                }
                else
                {
                    PostUpdateCommands.RemoveComponent<MoveBy>(entity);
                }
            }
        });
    }
}
