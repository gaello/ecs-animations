﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

/// <summary>
/// ECS component for MoveTo Tween.
/// </summary>
[System.Serializable]
public struct MoveTo : IComponentData
{
    // Animation Baseline
    [ReadOnly] public float Delay; // Animation Delay.
    [ReadOnly] public float Duration; // Animation Duration.
    [ReadOnly] public bool LoopAnimation; // Is animation looping?
    [ReadOnly] public AnimationCurveEnum AnimationCurve; // Animation curve used for this tween.

    public float ElapsedTime; // Animation Elapsed Time.

    // Move To
    [ReadOnly] public float3 StartValue;
    [ReadOnly] public float3 EndValue;
}
