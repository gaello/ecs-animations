﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

/// <summary>
/// ECS component for ScaleBy Tween.
/// </summary>
[System.Serializable]
public struct ScaleBy : IComponentData
{
    // Animation Baseline
    [ReadOnly] public float Delay; // Animation Delay.
    [ReadOnly] public float Duration; // Animation Duration.
    [ReadOnly] public bool LoopAnimation; // Is animation looping?
    [ReadOnly] public AnimationCurveEnum AnimationCurve; // Animation curve used for this tween.

    public float ElapsedTime; // Animation Elapsed Time.

    // Scale By
    [ReadOnly] public float3 TargetOffset;
}
