﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;

/// <summary>
/// This class generates the ECS animation example.
/// </summary>
public class ExampleGenerator : MonoBehaviour
{
    // Reference to mesh for sprite.
    [SerializeField]
    private Mesh spriteMesh;

    // References to materials for sprites.
    [SerializeField]
    private Material[] spriteMaterials;

    /// <summary>
    /// Unity method called on the first frame.
    /// </summary>
    void Start()
    {
        GenerateExample();
    }

    /// <summary>
    /// Method that generate example ECS.
    /// </summary>
    private void GenerateExample()
    {
        // Reference to Entity Manager.
        var entityManager = World.Active.EntityManager;

        // Generating Entity with scaling tween.
        var scaleEntity = entityManager.CreateEntity(
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(NonUniformScale),
                typeof(ScaleTo)
            );

        // Assigning values to the entity with scale tween.
        entityManager.SetSharedComponentData(scaleEntity, new RenderMesh { mesh = spriteMesh, material = spriteMaterials[0] });
        entityManager.SetComponentData(scaleEntity, new Translation { Value = new float3(-2.5f, 0, 0) });
        entityManager.SetComponentData(scaleEntity, new ScaleTo
        {
            AnimationCurve = AnimationCurveEnum.Bounce,
            Duration = 3,
            StartValue = new float3(1),
            EndValue = new float3(3),
            LoopAnimation = true
        });

        // Generating Entity with hover tween.
        var hoverEntity = entityManager.CreateEntity(
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Hover)
            );

        // Assigning values to the entity with hover tween.
        entityManager.SetSharedComponentData(hoverEntity, new RenderMesh { mesh = spriteMesh, material = spriteMaterials[1] });
        entityManager.SetComponentData(hoverEntity, new Translation { Value = new float3(0, 0, 0) });
        entityManager.SetComponentData(hoverEntity, new Hover
        {
            AnimationCurve = AnimationCurveEnum.Hermite,
            Duration = 4,
            BottomValue = new float3(0, -1, 0),
            TopValue = new float3(0, 1, 0),
            LoopAnimation = true
        });

        // Generating Entity with rotate tween.
        var rotateEntity = entityManager.CreateEntity(
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(Rotation),
            typeof(RotateTo)
        );

        // Assigning values to the entity with rotate tween.
        entityManager.SetSharedComponentData(rotateEntity, new RenderMesh { mesh = spriteMesh, material = spriteMaterials[2] });
        entityManager.SetComponentData(rotateEntity, new Translation { Value = new float3(2, 0, 0) });
        entityManager.SetComponentData(rotateEntity, new RotateTo
        {
            AnimationCurve = AnimationCurveEnum.Hermite,
            Duration = 5,
            StartValue = new float3(0, 0, 0),
            EndValue = new float3(0, 0, 2 * math.PI),
            LoopAnimation = true
        });
    }
}
