# ECS Tweens and Animations in Unity

So you are looking for information on how to do a little animations in ECS in Unity? That's easy! This repository contain what you looking for!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/07/11/ecs-animations-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can do tweens and animations with ECS in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/ecs-animations/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

Now you know how to do tweens and animations in ECS in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
